package com.weanbee.sampleproject.service;

import com.weanbee.sampleproject.dao.SampleUserDao;
import com.weanbee.sampleproject.entity.SimpleUser;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class SampleService {

    @EJB
    private SampleUserDao sampleUserDao;

    public void sayHello() {
        System.out.println("Hello World :-) !");
    }

    public void printAllUsers() {
        List<SimpleUser> allUsers = sampleUserDao.getAllUsers();
        System.out.println("There are " + allUsers.size() + " in database !");
    }

    public void addSimpleUser() {
        SimpleUser user = new SimpleUser();
        user.setUsername("Benjamin - " + new Date().getTime());
        sampleUserDao.addUser(user);
    }
}

package com.weanbee.sampleproject.dao.impl;

import com.weanbee.sampleproject.dao.SampleUserDao;
import com.weanbee.sampleproject.entity.SimpleUser;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaSampleUserDao implements SampleUserDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<SimpleUser> getAllUsers() {
        TypedQuery<SimpleUser> q = em.createQuery("select u FROM SimpleUser AS u", SimpleUser.class);
        return q.getResultList();
    }

    @Override
    public SimpleUser addUser(SimpleUser user) {
        em.persist(user);
        return user;
    }
}

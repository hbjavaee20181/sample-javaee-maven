package com.weanbee.sampleproject.dao;

import com.weanbee.sampleproject.entity.SimpleUser;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Local
public interface SampleUserDao {

    List<SimpleUser> getAllUsers();

    SimpleUser addUser(SimpleUser user);
}

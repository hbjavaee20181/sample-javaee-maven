package com.weanbee.sampleproject.servlet;

import com.weanbee.sampleproject.service.SampleService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ben on 15/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebServlet(urlPatterns = "/sample")
public class SampleServlet extends HttpServlet {

    @EJB
    private SampleService sampleService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        sampleService.sayHello();
        sampleService.printAllUsers();
        sampleService.addSimpleUser();
        req.setAttribute("name", "Benjamin");
        req.getRequestDispatcher("/sample.jsp").forward(req, resp);
    }
}
